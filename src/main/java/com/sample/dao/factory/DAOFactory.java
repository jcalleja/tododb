package com.sample.dao.factory;

import com.sample.dao.TodoUserDAO;

public interface DAOFactory {

	public TodoUserDAO getTodoUserDAO();

}
