package com.sample.dao.factory;

import javax.sql.DataSource;

import com.sample.dao.TodoUserDAO;
import com.sample.dao.postgre.PostgreTodoUserDAO;

public class PostgreDAOFactory implements DAOFactory {
	
	private DataSource dataSource;
	
	public PostgreDAOFactory() {
	}

	public PostgreDAOFactory(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public TodoUserDAO getTodoUserDAO() {
		TodoUserDAO dao = new PostgreTodoUserDAO(dataSource);
		return dao;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
}
