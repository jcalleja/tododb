package com.sample.dao;

import com.sample.data.TodoUser;

public interface TodoUserDAO {
	
	/**
	 * 
	 * @param email
	 * @return the TodoUser with the given email
	 */
	public TodoUser findTodoUserByEmail(String email);
	
	/**
	 * 
	 * @param id
	 * @return the TodoUser with the given id
	 */
	public TodoUser findTodoUserById(long id);

	/**
	 * 
	 * @param user
	 * @return TodoUser passed in with any db generated values filled in (e.g. the db generated id)
	 */
	public TodoUser insertTodoUser(TodoUser user);
	
	/**
	 * 
	 * @param user
	 * @return the number of rows updated
	 */
	public int updateTodoUser(TodoUser user);

	/**
	 * 
	 * @param email
	 * @return the number of rows deleted
	 */
	public int deleteTodoUserByEmail(String email);

	/**
	 * 
	 * @param id
	 * @return the number of rows deleted
	 */
	public int deleteTodoUserById(long id);
}
