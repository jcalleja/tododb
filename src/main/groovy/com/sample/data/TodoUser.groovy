package com.sample.data

import org.apache.commons.lang.builder.ToStringBuilder

class TodoUser {
	long id;
	String email;
	String password;
	String confirmationUri;
	boolean registered;

	def propertyMissing(String name, value) {
		if(isConfirmationUri(name)) {
			this.confirmationUri = value
		} else {
			unknownProperty(name)
		}
	}

	def propertyMissing(String name) {
		if(isConfirmationUri(name)) {
			return confirmationUri
		} else {
			unknownProperty(name)
		}
	}
	
	def isConfirmationUri(String name) {
		'confirmation_uri'.equals(name)
	}
	
	def unknownProperty(String name) {
		throw new MissingPropertyException(name, this.class)
	}
	
	@Override
	public String toString() {
		ToStringBuilder.reflectionToString(this);
	}
}
