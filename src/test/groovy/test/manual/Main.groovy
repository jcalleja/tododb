package test.manual

import groovy.sql.Sql

import javax.sql.DataSource

import org.springframework.context.support.GenericXmlApplicationContext

class Main {

	static main(args) {
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
		// populates db and initializes dataSource pool
		ctx.load("classpath:testContext.xml");
		ctx.refresh();

		DataSource dataSource = ctx.getBean("dataSource", javax.sql.DataSource.class);
		Sql sql = new Sql(dataSource)
		
		sql.eachRow("SELECT * FROM todouser") { user ->
			println user.email
			println user.password
			println user.registered
			println user.confirmationUrl
			println '-' * 20
		}
		
		println ''
		findTodoUserByEmail(sql, 'anon@gmail.com')

	}
	
	static void findTodoUserByEmail(Sql sql, String email) {
		Object o = sql.firstRow("""SELECT * FROM todouser WHERE email = $email""")
		println "result of findTodoUserByEmail: $o"
	}

}
