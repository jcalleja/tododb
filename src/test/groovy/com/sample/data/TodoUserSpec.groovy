package com.sample.data

import spock.lang.Specification

class TodoUserSpec extends Specification {
	
	TodoUser user
	String testUri = "http://blah.com"

	def setup() {
		user = new TodoUser()
	}
	
	def "setting confirmationUri normally works"() {
		when: user.confirmationUri = testUri
		then: user.confirmationUri == testUri
	}

	def "set/get confirmationUri using confirmation_uri table name works"() {
		when: user.confirmation_uri = testUri
		
		then:
		user.confirmationUri == testUri
		user.confirmation_uri == testUri
	}

	def "can construct new TodoUser from map"() {
		given: 'a map (or GroovyRowResult, same thing) retrieved with a SELECT statement'
		def queryResultMap = [id: 1, email: 'blah@gmail.com', password: 'supersecret', confirmation_uri: 'abc', registered: false]

		when: 'using the map to construct a new TodoUser'
		user = new TodoUser(queryResultMap)
		
		then: "keys in map are mapped to TodoUser property names including the key 'confirmation_uri' which is mapped to the property confirmationUri by TodoUser"
		user.id == queryResultMap.id
		user.email == queryResultMap.email
		user.password == queryResultMap.password
		user.confirmationUri == queryResultMap.confirmation_uri
		user.registered == queryResultMap.registered
	}
	
	def "setting an undefined/unmapped property throws MissingPropertyException"() {
		given: 'a property which is not defined or mapped to a defined property in TodoUser'
		def undefinedProperty = 'confirmationURI'
		
		when: 'setting the undefined property on a TodoUser'
		user."$undefinedProperty" = testUri
		
		then: 'a MissingPropertyException ex will be thrown'
		MissingPropertyException ex = thrown()

		and: 'ex.type is the type on which the property was attempted to be called'
		ex.type == TodoUser.class

		and: 'ex.property is the name of the property that could not be found i.e. the undefined property'
		ex.property == undefinedProperty
	}

	def "getting an undefined/unmapped property throws MissingPropertyException"() {
		given: 'a property which is not defined or mapped to a defined property in TodoUser'
		def undefinedProperty = 'confirmationURI'
		
		when: 'getting the undefined property on a TodoUser'
		def p = user."$undefinedProperty"
		
		then: 'a MissingPropertyException ex will be thrown'
		MissingPropertyException ex = thrown()

		and: 'ex.type is the type on which the property was attempted to be called'
		ex.type == TodoUser.class

		and: 'ex.property is the name of the property that could not be found i.e. the undefined property'
		ex.property == undefinedProperty
	}
}
